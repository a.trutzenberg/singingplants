#include <Adafruit_NeoPixel.h>


// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(3, 2, NEO_GRB + NEO_KHZ800);
    

void setup() {
  strip.begin();
  strip.setBrightness(30);
  strip.show();
}

void loop() {
  strip.setPixelColor(0, 255, 255, 255);
  strip.setPixelColor(1, 0, 255, 80);
  strip.setPixelColor(2, 0, 0, 255);
  strip.show();
  

}
