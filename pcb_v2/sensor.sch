EESchema Schematic File Version 4
LIBS:power
LIBS:device
LIBS:74xx
LIBS:audio
LIBS:interface
LIBS:sensor-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Plant sensor"
Date "2018-12-08"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U1
U 1 1 5C0C143D
P 3100 3800
F 0 "U1" H 2570 3846 50  0000 R CNN
F 1 "ATtiny85-20PU" H 2570 3755 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3100 3800 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C1
U 1 1 5C0C1595
P 1650 3850
F 0 "C1" H 1828 3896 50  0000 L CNN
F 1 "100nF" H 1828 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1650 3850 50  0001 C CNN
F 3 "" H 1650 3850 50  0001 C CNN
	1    1650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3200 1900 3200
Wire Wire Line
	1900 3200 1900 3600
Wire Wire Line
	1900 3600 1650 3600
Connection ~ 1650 3600
Wire Wire Line
	1650 4100 1650 4400
Connection ~ 1650 4400
Wire Wire Line
	1650 4400 1650 4450
Connection ~ 3100 4400
$Comp
L pspice:CAP C2
U 1 1 5C0C1806
P 4200 4350
F 0 "C2" H 4378 4396 50  0000 L CNN
F 1 "100nF" H 4378 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4200 4350 50  0001 C CNN
F 3 "" H 4200 4350 50  0001 C CNN
	1    4200 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4100 4200 3800
Wire Wire Line
	4200 3800 3700 3800
Connection ~ 4200 4600
$Comp
L power:+5V #PWR03
U 1 1 5C0C1B5E
P 5050 4000
F 0 "#PWR03" H 5050 3850 50  0001 C CNN
F 1 "+5V" H 5065 4173 50  0000 C CNN
F 2 "" H 5050 4000 50  0001 C CNN
F 3 "" H 5050 4000 50  0001 C CNN
	1    5050 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C0C1BC1
P 5600 4600
F 0 "#PWR04" H 5600 4350 50  0001 C CNN
F 1 "GND" H 5605 4427 50  0000 C CNN
F 2 "" H 5600 4600 50  0001 C CNN
F 3 "" H 5600 4600 50  0001 C CNN
	1    5600 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5C0C1C0F
P 5050 4250
F 0 "R4" H 5120 4296 50  0000 L CNN
F 1 "22k" H 5120 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4980 4250 50  0001 C CNN
F 3 "~" H 5050 4250 50  0001 C CNN
	1    5050 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5C0C1C9C
P 5200 4600
F 0 "R5" V 4993 4600 50  0000 C CNN
F 1 "22k" V 5084 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5130 4600 50  0001 C CNN
F 3 "~" H 5200 4600 50  0001 C CNN
	1    5200 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 4600 4550 4600
Wire Wire Line
	5050 4600 5050 4400
Connection ~ 5050 4600
Wire Wire Line
	5050 4100 5050 4050
$Comp
L Device:LED D1
U 1 1 5C0C2364
P 4400 3450
F 0 "D1" H 4391 3666 50  0000 C CNN
F 1 "LED" H 4391 3575 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 4400 3450 50  0001 C CNN
F 3 "~" H 4400 3450 50  0001 C CNN
	1    4400 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C0C23EB
P 4000 3200
F 0 "R2" H 4070 3246 50  0000 L CNN
F 1 "100" H 4070 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3930 3200 50  0001 C CNN
F 3 "~" H 4000 3200 50  0001 C CNN
	1    4000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3600 3850 3600
Wire Wire Line
	4000 3600 4000 3350
Wire Wire Line
	4000 3050 4250 3050
Wire Wire Line
	4250 3050 4250 3450
Wire Wire Line
	4550 3450 4550 4600
Connection ~ 4550 4600
$Comp
L LED:WS2812B D4
U 1 1 5C0C29E3
P 5400 3250
F 0 "D4" V 5446 2909 50  0000 R CNN
F 1 "WS2812B" V 5355 2909 50  0000 R CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5450 2950 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5500 2875 50  0001 L TNN
	1    5400 3250
	0    -1   -1   0   
$EndComp
$Comp
L LED:WS2812B D3
U 1 1 5C0C2A99
P 5400 2650
F 0 "D3" V 5446 2309 50  0000 R CNN
F 1 "WS2812B" V 5355 2309 50  0000 R CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5450 2350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5500 2275 50  0001 L TNN
	1    5400 2650
	0    -1   -1   0   
$EndComp
$Comp
L LED:WS2812B D2
U 1 1 5C0C2BCF
P 5400 2050
F 0 "D2" V 5446 1709 50  0000 R CNN
F 1 "WS2812B" V 5355 1709 50  0000 R CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5450 1750 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5500 1675 50  0001 L TNN
	1    5400 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R6
U 1 1 5C0C305B
P 5400 3700
F 0 "R6" H 5470 3746 50  0000 L CNN
F 1 "100" H 5470 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5330 3700 50  0001 C CNN
F 3 "~" H 5400 3700 50  0001 C CNN
	1    5400 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3850 3700 3850
Wire Wire Line
	3700 3850 3700 3900
Wire Wire Line
	5700 2050 5700 2650
Wire Wire Line
	5350 4600 5600 4600
Connection ~ 5700 2650
Wire Wire Line
	5700 2650 5700 3250
Connection ~ 5700 3250
Wire Wire Line
	5700 3250 5700 4600
Connection ~ 5600 4600
Wire Wire Line
	5600 4600 5700 4600
Wire Wire Line
	5100 2050 5100 2650
Wire Wire Line
	5100 4050 5050 4050
Connection ~ 5100 2650
Wire Wire Line
	5100 2650 5100 3250
Connection ~ 5100 3250
Wire Wire Line
	5100 3250 5100 4050
Connection ~ 5050 4050
Wire Wire Line
	5050 4050 5050 4000
$Comp
L Device:Buzzer BZ1
U 1 1 5C0C4CB5
P 3900 2700
F 0 "BZ1" H 4053 2729 50  0000 L CNN
F 1 "Buzzer" H 4053 2638 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_12x9.5RM7.6" V 3875 2800 50  0001 C CNN
F 3 "~" V 3875 2800 50  0001 C CNN
	1    3900 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5C0C4D65
P 4700 2950
F 0 "R3" H 4770 2996 50  0000 L CNN
F 1 "100" H 4770 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 2950 50  0001 C CNN
F 3 "~" H 4700 2950 50  0001 C CNN
	1    4700 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3500 3700 2800
Wire Wire Line
	3700 2800 3800 2800
Wire Wire Line
	4000 2800 4700 2800
Wire Wire Line
	4700 3100 4700 4600
Wire Wire Line
	4550 4600 4700 4600
Connection ~ 4700 4600
Wire Wire Line
	4700 4600 5050 4600
Wire Wire Line
	3850 3600 3850 3050
Wire Wire Line
	3850 3050 2450 3050
Wire Wire Line
	2450 3050 2450 2300
Connection ~ 3850 3600
Wire Wire Line
	3850 3600 4000 3600
Wire Wire Line
	3700 3700 3750 3700
Wire Wire Line
	3750 3700 3750 3100
Wire Wire Line
	3750 3100 2250 3100
$Comp
L Device:R_POT RV1
U 1 1 5C0C7D65
P 2200 2750
F 0 "RV1" H 2130 2796 50  0000 R CNN
F 1 "R_POT" H 2130 2705 50  0000 R CNN
F 2 "Potentiometer_SMD:Potentiometer_Bourns_3214J_Horizontal" H 2200 2750 50  0001 C CNN
F 3 "~" H 2200 2750 50  0001 C CNN
	1    2200 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2200 2900 2200 3100
$Comp
L Device:R R1
U 1 1 5C0CAD26
P 2250 3450
F 0 "R1" H 2180 3404 50  0000 R CNN
F 1 "10k" H 2180 3495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2180 3450 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	2250 3300 2250 3100
Connection ~ 2250 3100
Wire Wire Line
	2250 3100 2200 3100
Wire Wire Line
	2250 3600 2250 4400
Wire Wire Line
	1650 4400 2250 4400
Connection ~ 2250 4400
Wire Wire Line
	2250 4400 2850 4400
Wire Wire Line
	3100 5650 3400 5650
Wire Wire Line
	3400 4600 4200 4600
Wire Wire Line
	3100 4400 3100 5650
$Comp
L SJ-3524-SMT:SJ-3524-SMT J2
U 1 1 5C167B3F
P 3900 5450
F 0 "J2" H 3570 5454 50  0000 R CNN
F 1 "SJ-3524-SMT" H 3570 5545 50  0000 R CNN
F 2 "SJ-3524-SMT:CUI_SJ-3524-SMT" H 3900 5450 50  0001 L BNN
F 3 "Unavailable" H 3900 5450 50  0001 L BNN
F 4 "None" H 3900 5450 50  0001 L BNN "Feld4"
F 5 "None" H 3900 5450 50  0001 L BNN "Feld5"
F 6 "SJ-3524-SMT-TR" H 3900 5450 50  0001 L BNN "Feld6"
F 7 "SJ Series 3.5 mm 12 V SMT Right Angle Stereo Audio Jack" H 3900 5450 50  0001 L BNN "Feld7"
F 8 "https://www.cui.com/product/interconnect/audio-connectors/3.5-mm-jacks/sj-352x-smt-series?utm_source=snapeda.com&utm_medium=referral&utm_campaign=snapedaBOM" H 3900 5450 50  0001 L BNN "Feld8"
F 9 "CUI Inc." H 3900 5450 50  0001 L BNN "Feld9"
	1    3900 5450
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 4600 3400 5350
$Comp
L 1825910-7:1825910-7 S1
U 1 1 5C165B42
P 2750 4850
F 0 "S1" V 2704 5080 50  0000 L CNN
F 1 "1825910-7" V 2795 5080 50  0000 L CNN
F 2 "1825910-7:SW_1825910-7" H 2750 4850 50  0001 L BNN
F 3 "TE Connectivity" H 2750 4850 50  0001 L BNN
F 4 "https://www.te.com/usa-en/product-1825910-7.html?te_bu=Cor&te_type=disp&te_campaign=seda_glo_cor-seda-global-disp-prtnr-fy19-seda-model-bom-cta_sma-317_1&elqCampaignId=32493" H 2750 4850 50  0001 L BNN "Feld4"
F 5 "None" H 2750 4850 50  0001 L BNN "Feld5"
F 6 "Switch Tactile OFF _ON_ SPST Round Button PC Pins 0.05A 24VDC 100000Cycle 2.55N Thru-Hole Loose Piece" H 2750 4850 50  0001 L BNN "Feld6"
F 7 "1825910-7" H 2750 4850 50  0001 L BNN "Feld7"
F 8 "Unavailable" H 2750 4850 50  0001 L BNN "Feld8"
F 9 "None" H 2750 4850 50  0001 L BNN "Feld9"
F 10 "1825910-7" H 2750 4850 50  0001 L BNN "Feld10"
	1    2750 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 4450 2850 4400
Connection ~ 2850 4400
Wire Wire Line
	2850 4400 3100 4400
Wire Wire Line
	2850 5250 3250 5250
Wire Wire Line
	3250 5250 3250 4450
Wire Wire Line
	3250 4450 3700 4450
Wire Wire Line
	3700 4450 3700 4000
Wire Wire Line
	2050 2750 2050 2300
Text Label 2050 2300 0    50   ~ 0
worm1
Text Label 2450 2300 0    50   ~ 0
worm2
$Comp
L BAT-HLD-001:BAT-HLD-001 U2
U 1 1 5C2389FB
P 1100 4250
F 0 "U2" H 1100 4250 50  0001 L BNN
F 1 "BAT-HLD-001" H 1100 4250 50  0001 L BNN
F 2 "BAT-HLD-001:BAT-HLD-001" H 1100 4250 50  0001 L BNN
F 3 "None" H 1100 4250 50  0001 L BNN
F 4 "None" H 1100 4250 50  0001 L BNN "Feld4"
F 5 "BAT-HLD-001-TR" H 1100 4250 50  0001 L BNN "Feld5"
F 6 "Unavailable" H 1100 4250 50  0001 L BNN "Feld6"
F 7 "Linx Technologies" H 1100 4250 50  0001 L BNN "Feld7"
F 8 "CR2032 Battery Holder Surface Mount Tape and Reel" H 1100 4250 50  0001 L BNN "Feld8"
	1    1100 4250
	1    0    0    -1  
$EndComp
$Comp
L PCM12SMTR:PCM12SMTR SW1
U 1 1 5C238C31
P 1000 3750
F 0 "SW1" V 1046 3422 50  0000 R CNN
F 1 "PCM12SMTR" V 955 3422 50  0000 R CNN
F 2 "PCM12SMTR:SW_PCM12SMTR" H 1000 3750 50  0001 L BNN
F 3 "https://www.digikey.de/product-detail/en/c-k/PCM12SMTR/401-2016-1-ND/1640125?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 1000 3750 50  0001 L BNN
F 4 "DO-201 C&amp;K Components" H 1000 3750 50  0001 L BNN "Feld4"
F 5 "PCM12SMTR" H 1000 3750 50  0001 L BNN "Feld5"
F 6 "401-2016-1-ND" H 1000 3750 50  0001 L BNN "Feld6"
F 7 "Switch Slide ON ON SPDT Side Slide 0.3A 6VDC 10000Cycles Gull Wing SMD T/R" H 1000 3750 50  0001 L BNN "Feld7"
F 8 "C&K" H 1000 3750 50  0001 L BNN "Feld8"
	1    1000 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1000 3350 1000 3250
Wire Wire Line
	1000 3250 1650 3250
Wire Wire Line
	1650 3250 1650 3600
Wire Wire Line
	1100 4450 1650 4450
$EndSCHEMATC
