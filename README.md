# Singing Plants
### A Digital Biology Project

Welcome to our Repository!

Here we document the development of our "Singing Plants" project. We aim to host a Singing Plant workshop at re:publica 2019.


For now this is just a collection of resources but we will fill this space with more information at a later point <3

- [Headphone jack pin out](http://www.circuitbasics.com/how-to-hack-a-headphone-jack/)
- [Audio Bootloader](https://github.com/vektorious/TinyAudioBoot)
- [Audio IDE integration](https://github.com/8BitMixtape/8Bit-Mixtape-NEO/wiki/4_3-IDE-integration)
- [Micro Coconut NEO schematics](https://github.com/8BitMixtape/8Bit-Mixtape-NEO/wiki/2_2-Micro-Coconut-NEO)
- s[Gär Lämpli Hacktaria page](http://wlu18www30.webland.ch/wiki/G%C3%A4r_L%C3%A4mpli)
